const express = require('express');
const app = express();
const http = require('http');
const mongoose = require('mongoose');
const hostname = '127.0.0.1';
const bodyParser = require('body-parser');
const emailSender = require('./API/controllers/email-sender');

//Swagger for api docs
var swaggerOptions = require('./config/swagger.js');
const expressSwagger = require('express-swagger-generator')(app);
expressSwagger(swaggerOptions);

//Routes
const usersRoutes = require('./api/routes/users');
const notificationsRoutes = require('./api/routes/notifications');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/api/users', usersRoutes);
app.use('/api/notifications', notificationsRoutes);

//DB Connection
const uri =  process.env.MONGO_URI;
mongoose.connect(uri).then(
    () => {
        console.log("Database connection established!");
    },
    err => {
        console.log("Error connecting Database instance due to: ", err);
    }
);
mongoose.Promise = global.Promise;

async function run() {
    // Create a new mongoose model
    const User = mongoose.model('User')
    // Create a change stream. The 'change' event gets emitted when there's a
    // change in the database
    User.watch().
    on('change', data => {
        if(data.operationType === 'insert'){
            emailSender(data.fullDocument.name);
        }

    });

}

//const socket = require('./API/controllers/socket');
//Header Protection (For Browsers)

app.use((req, res, next)=>{
    res.header("Access-Control-Allow-Origin", '*');
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods', 'POST, PATCH, GET, DELETE');
        return res.status(200).json({});
    }
    return next();
});

//Start Error Handling from all requests (middleware)
app.use((req, res, next)=>{
    const error = new  Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) =>{
    res.status(error.status || 500);
    res.json({
        error:{
            message: error.message
        }
    });
});
//End of Error Handling

run();

module.exports = app;