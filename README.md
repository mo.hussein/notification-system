# Notification System

Simple notifications system used to notify specific user or group of users.

There are two types of notifications in the system:
-Send a group notification trough the API 
-The system listen to user insertion to database to send welcome message.


## Development server

Run `node email-receiver` & `node email-receiver "customers.*"`  for watch the background services of receiving messages. (customers.*)
is the routing key you can add more than one routing key (* will match one ore more words in routing key).
For starting server side Run `node server` or `npm start`.


## Overview

Notifications System API using Node.js (Express.js) and Mongoose.

MongoDB Change Streams used for listening to database changes.
Change streams allow applications to access real-time data changes without the complexity and risk of tailing the oplog. Applications can use change streams to subscribe to all data changes on a single collection,
 a database, or an entire deployment, and immediately react to them. Because change streams use the aggregation framework,
 applications can also filter for specific changes or transform the notifications at will. Change streams are available for replica sets and sharded clusters.
 
Notification system use RabbitMQ (message broker) for handling number of requests per minute limitations, and ensure delivery of message.

## API Refrence
Swagger API documentation will be found at `http://localhost:3001/api-docs`


