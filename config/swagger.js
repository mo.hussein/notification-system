const options = {
  swaggerDefinition: {
    info: {
      description: 'Notifications API',
      title: 'Swagger',
      version: '1.0.0'
    },
    host: 'localhost:3001',
    basePath: '/api',
    produces: [
      'application/json',
      'application/xml'
    ],
    schemes: ['http']
  },
  basedir: __dirname, // app absolute path
  files: ['../API/routes/users.js', '../API/routes/notifications.js'] // Path to the API handle folder
}

module.exports = options
