const express = require('express');
const router = express.Router();

const notificationsController = require('../controllers/notifications');

/**
 * @route POST /notifications
 * @group Create - push new notification
 * @param {string} message.body.required - message
 * @param {string} group.body.required - group you want to send
 * @param {string} key.body.required - group+Key = topic
 * @returns  200
 */
router.post('/', notificationsController.send);
module.exports = router;