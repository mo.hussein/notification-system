const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();

const usersController = require('../controllers/users');

/**
 * @route GET /users
 * @group Read - View users
 * @returns {object} 200 - An object with the key 'message': All users fetched!, 'users': array of fetched users, 'count': length of users array
 * @returns {Error}  404 - Error fetching users
 */
router.get('/', usersController.users_get_all);


/**
 * @route POST /users
 * @group Create - create new user
     * @param {string} name.body.required - name
 * @param {string} phone.body.required - phone
 * @param {string} email.body.required - email
 * @param {string} registration_token.body.required - registration token
 * @returns {object} 201 - An object with key 'message':"User "+result.name+" Created!", 'user':{object}
 * @returns {Error}  409 - Email already exists
 * @returns {Error}  500 - "error": "User validation failed"
 */
router.post('/', usersController.save);




module.exports = router;