var amqp = require('amqplib/callback_api');

var send = function(topic, message) {
    amqp.connect('amqp://localhost', function (error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                throw error1;
            }
            var exchange = 'SMS';
            var key = topic;
            var msg = message;

            channel.assertExchange(exchange, 'topic', {
                durable: false
            });
            channel.publish(exchange, key, Buffer.from(msg));
            console.log(" [x] Sent %s:'%s'", key, msg);
        });

        setTimeout(function () {
            connection.close();
            process.exit(0)
        }, 500);
    });
}

module.exports = send;