const sendGroup = require('./send_group');

exports.send = (req, res, next)=> {
    var msg = req.body.message;
    var group = req.body.group;
    var key = req.body.key;
    var topic = group+'.'+key;
    sendGroup(topic, msg);
};
