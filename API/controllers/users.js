const mongoose = require('mongoose');
const User = require('../models/user');


exports.users_get_all = (req, res, next)=> {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    User.find().select("-__v").exec().then(docs => {
        if(docs.length >= 0)
        {
            const response = {
                count: docs.length,
                users: docs.map(doc => {
                    return{
                        name: doc.name,
                        phone: doc.phone,
                        email: doc.email,
                        token: doc.registration_token,
                    }
                }),
                message: "All users fetched!"
            }
            res.status(200).json(response);
        }else{
            res.status(200).json({
                message:'no data'
            })
        }

    }).catch(error => {
        res.status(404).json({
            message:'get all users',
            error:error
        })
    })
};

exports.save = (req, res, next)=>{
    User.findOne({email: req.body.email})
        .exec()
        .then(user =>{
            if(user != null){
                console.log(user);
                return res.status(409).json({
                    message:"Email already exists"
                }); //conflict
            }else{
                const user = new User({
                    _id: new mongoose.Types.ObjectId(),
                    name: req.body.name,
                    email: req.body.email,
                    phone: req.body.phone,
                    registration_token: req.body.token
                    ///Assign group rndm
                });
                user.save()
                    .then(result =>{

                        res.status(201).json({
                            message:"User "+result.name+" Created!",
                            user: {
                                _id: result._id,
                                name: result.name,
                            }
                        });

                    })
                    .catch(error => {
                        res.status(500).json({
                            error: error._message
                        });
                    });

            }
        })
        .catch(error=>{
            return res.status(500).json({error: error})
        });

};

